# Node, Ruby & Python Docker Container

Docker container to use for development with:

- node 10.15.3
- ruby 6.3.0
- python 3.7.3
- pip 19.1.1
- awscli
- awsebcli
